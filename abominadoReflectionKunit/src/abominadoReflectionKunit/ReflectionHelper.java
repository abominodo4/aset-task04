package abominadoReflectionKunit;

public class ReflectionHelper {

	public long x = 5;
	private long y = 10;
	public long z = 15;

	public ReflectionHelper() {
	  }

	public ReflectionHelper(long l, long m,long n) {
	    this.x = l;
	    this.y = m;
	    this.z = n;
	  }

	public void squareX() {
		this.x *= this.x;
	}

	public void squareY() {
		this.y *= this.y;
	}

	public void squareZ() {
		this.z *= this.z;
	}

	public long getX() {
		return x;
	}

	public void setX(long l) {
		this.x = l;
	}

	public long getY() {
		return y;
	}

	public void setY(long m) {
		this.y = m;
	}

	public long getZ() {
		return z;
	}

	public void setZ(long n) {
		this.z = n;
	}

	public String toString() {
		return String.format("(l:%d, m:%d, n:%d)", x, y, z);
	}

}
